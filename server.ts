import cors from 'cors'
import express, { NextFunction, Request, Response } from 'express'
import { print } from 'listening-on'
import path from 'path'

let app = express()

app.use(cors())

app.get('/', (req, res) => {
  res.end(/* html */ `
	<form action='/add' method='get'>
	  <input name='a'>
		+
	  <input name='b'>
		<input type='submit'>
	</form>
	`)
})

app.get('/add', (req, res) => {
  let a = getQueryInt(req, 'a')
  let b = getQueryInt(req, 'b')
  let c = a + +b
  res.end(/* html */ `
	<div>
	  ${a} + ${b} = ${c}
	</div>
	<a href='/'>Again</a>
	`)
})

// app.set('views', path.join(__dirname, 'views'))
// app.set('view engine', 'ejs')

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  if (req.headers.accept?.includes('text/html')) {
    // res.render('error', { err })
    next(err)
    return
  }
  if (err instanceof HttpError) {
    res.status(err.statusCode).json({ error: err.message })
  } else if (err instanceof Error) {
    res.status(500).json({ error: err.message })
  } else {
    res.status(500).json({ error: String(err) })
  }
})

function getQueryInt(req: Request, field: string): number {
  if (!(field in req.query)) {
    throw new HttpError(400, `Missing query '${field}'`)
  }
  let str = req.query[field]
  if (!str) {
    throw new HttpError(400, `Expect non-empty query '${field}'`)
  }
  let num = +str
  if (!Number.isInteger(num)) {
    throw new HttpError(400, `Expect int query '${field}'`)
  }
  return num
}

class HttpError extends Error {
  constructor(public statusCode: number, public message: string) {
    super(message)
  }
}

let port = 8100
app.listen(port, () => {
  print(port)
})
